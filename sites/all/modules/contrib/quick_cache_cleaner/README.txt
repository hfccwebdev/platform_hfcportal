Quick Cache Cleaner 1.x for Drupal 7.x
--------------------------------------

Add an administrative menu item to quickly clear all Drupal core and Views
caches.

Installation
------------

Quick Cache Cleaner can be installed like any other Drupal module -- place it
in the sites/all/modules/contrib directory for your site and enable it from
the modules administration page.
